<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function bellespuces_pre_typo(?string $texte): string {
	if (is_null($texte)) {
		$texte = '';
	}
	if (strpos($texte, "\n") !== false) {
		$texte = preg_replace('/^-\s?(?!\*|#|-)/m', '-* ', $texte);
	}
	return $texte;
}
