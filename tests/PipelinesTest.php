<?php

namespace Spip\Bellespuces\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers bellespuces_pre_typo()
 * @internal
 */

class PipelinesTest extends TestCase {

	public static function dataPreTypo() {
		return [
			'null' => [
				'',
				null,
			],
			'empty' => [
				'',
				'',
			],
			'Multiligne' => [
				"-* Ligne 1\n"
				. '-* Ligne 2',
				"- Ligne 1\n"
				. '- Ligne 2',
			],
			'monoligne' => [
				'- Nombre negatif',
				'- Nombre negatif',
			],
		];
	}

	/**
	 * @dataProvider dataPreTypo
	 */
	public function testPreTypo($expected, $provided) {
		$actual = bellespuces_pre_typo($provided);
		$this -> assertSame($expected, $actual);
	}

}
