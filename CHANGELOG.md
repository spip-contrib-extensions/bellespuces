# Changelog

## 2.0.0 - 2024-10-13

### Fixed

- #1 Ne pas appliquer sur les champs monolignes

### Changed

- Les textes `null` sont transformés en texte `''`
- Typage php de `bellespuces_pre_typo()`

### Removed

- Compatibilité SPIP < 4.1
- Logo PNG

